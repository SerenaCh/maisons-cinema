import styles from "../../styles/Person.module.css";
import Head from "next/head";
import Error from "next/error";
import Layout from "../../components/layout";
import Image from "next/image";
import Film from "../../components/film";
import {getPerson} from "../../api/person";

const basePath = `${process.env.NEXT_PUBLIC_CINE_API}/images/personne`;

const Director = ({ director }) => {

  return director.isObjEmpty || !director.content ? (
    <Error statusCode={503} />
  ) : (
    <Layout>
      <Head>
        <title>{director.title}</title>
      </Head>
      <div className={styles.pageContent}>
        <section className={styles.personContainer}>
          <aside className={styles.aside1}>
            <Image
              src={`${basePath}/${
                director.content.photo ? director.content.photo : ""
              }`}
              alt={director.title}
              width="386"
              height="513"
              style={{ borderRadius: "1%" }}
            />
          </aside>
          <aside className={styles.aside2}>
            <div className={styles.titles}>
              <div>{director.content.profession}</div>
              <div>{director.title}</div>
            </div>
            <div>
              <p className={styles.description}>
                {director.content.commentaire}
              </p>
            </div>
            <div className={styles.bottomAside}>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>NATIONALITE</span>
                  <br />
                  <span>{director.content.nationalite}</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>WEB</span>
                  <br />
                  <span>
                    <a href={director.content.url_dbpedia}>dbpedia</a>
                  </span>
                </div>
              </div>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>PAYS DE NAISSANCE</span>
                  <br />
                  <span>{director.content.lieu_naissance}</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>DATE DE NAISSANCE</span>
                  <br />
                  <span>{director.content.date_naissance}</span>
                </div>
              </div>
            </div>
          </aside>
        </section>
        <section className={styles.filmsSection}>
          <div>Filmographie</div>
          <div className={styles.filmsCollection}>
            {Object.keys(director.content.movies).map((movieId, index) => (
              <Film key={index} movieId={movieId} movie={director.content.movies[movieId]}/>
            ))}
          </div>
        </section>
      </div>
    </Layout>
  );
};

export async function getStaticPaths() {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_CINE_API}/rubrique/directeurs`
  );
  const directors = await res.json();

  const paths = directors.persons.map((person) => ({
    params: { id: person.id.toString() },
  }));
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  return {
    props: { director: await getPerson(params.id) },
  };
}

export default Director;
