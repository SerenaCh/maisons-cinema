import Head from "next/head";
import Link from "next/link";
import Error from "next/error";
import { useRouter } from "next/router";
import styles from "../styles/Rubrique.module.css";
import Layout from "../components/layout";

const Producers = ({ producers, page }) => {
  const router = useRouter();

  const handlePreviousPage = (e) => {
    e.preventDefault();
    router.push(`actors?page=${Number(page) - 1}`);
  };

  const handleNextPage = (e) => {
    e.preventDefault();
    router.push(`actors?page=${Number(page) + 1}`);
  };

  return producers.length === 0 ? (
    <Error statusCode={503} />
  ) : (
    <Layout>
      <Head>
        <title>Liste de producteurs</title>
      </Head>
      <div className={styles.pageContent}>
        {producers !== undefined &&
          producers.map((producer) => (
            <div className={styles.linkBloc} key={producer.id}>
              <Link href={`/producers/${producer.id}`} passHref>
                <a className={styles.single}>
                  <div>{producer.fullname}</div>
                </a>
              </Link>
            </div>
          ))}
        <div className={styles.buttons}>
          {page >= 2 ? (
            <a
              href={`producers?page=${Number(page) + -1}`}
              onClick={handlePreviousPage}
            >
              <div>Page précédente</div>
            </a>
          ) : (
            <a
              className={styles.hide}
              href={`producers?page=${Number(page) + -1}`}
              onClick={handlePreviousPage}
            >
              <div>Page précédente</div>
            </a>
          )}
          <a
            href={`producers?page=${Number(page) + 1}`}
            onClick={handleNextPage}
          >
            <div>Page suivante</div>
          </a>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query }) {
  let data;
  try {
    const response = await fetch(
      `${
        process.env.NEXT_PUBLIC_CINE_API
      }/rubrique/producteurs/limit/10/offset/${((query.page ?? 1) - 1) * 10}`
    );
    data = await response.json();
  } catch (error) {
    data = [];
  }

  return {
    props: { producers: data.persons, page: query.page ?? 1 },
  };
}

export default Producers;
