import styles from "../../styles/Person.module.css";
import Layout from "../../components/layout";
import Head from "next/head";
import Image from "next/image";
import Error from "next/error";
import { getPerson } from "../../api/person";
import Film from "../../components/film";

const basePath = `${process.env.NEXT_PUBLIC_CINE_API}/images/personne`;

const Actor = ({ actor }) => {
  return actor.isObjEmpty || !actor.content ? (
    <Error statusCode={503} />
  ) : 
    <Layout>
      <Head>
        <title>{actor.title}</title>
      </Head>
      <div className={styles.pageContent}>
        <section className={styles.personContainer}>
          <aside className={styles.aside1}>
            <Image
              src={`${basePath}/${
                actor.content.photo ? actor.content.photo : ""
              }`}
              alt={actor.title}
              width="386"
              height="513"
              style={{ borderRadius: "1%" }}
            />
          </aside>
          <aside className={styles.aside2}>
            <div className={styles.titles}>
              <div>{actor.content.profession}</div>
              <div>{actor.title}</div>
            </div>
            <div>
              <p className={styles.description}>{actor.content.commentaire}</p>
            </div>
            <div className={styles.bottomAside}>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>NATIONALITE</span>
                  <br />
                  <span>{actor.content.nationalite}</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>WEB</span>
                  <br />
                  <span>
                    <a href={actor.content.url_dbpedia}>dbpedia</a>
                  </span>
                </div>
              </div>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>PAYS DE NAISSANCE</span>
                  <br />
                  <span>{actor.content.lieu_naissance}</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>DATE DE NAISSANCE</span>
                  <br />
                  <span>{actor.content.date_naissance}</span>
                </div>
              </div>
            </div>
          </aside>
        </section>
        <section className={styles.filmsSection}>
          <div>Filmographie</div>
          <div className={styles.filmsCollection}>
            {Object.keys(actor.content.movies).map((movieId, index) => (
              <Film key={index} movieId={movieId} movie={actor.content.movies[movieId]}/>
            ))}
          </div>
        </section>
      </div>
    </Layout>
};

export async function getStaticPaths() {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_CINE_API}/rubrique/acteurs`
  );
  const actors = await res.json();

  const paths = actors.persons.map((person) => ({
    params: { id: person.id.toString() },
  }));
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  return {
    props: { actor: await getPerson(params.id) },
  };
}

export default Actor;
