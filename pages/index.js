import styles from "../styles/Home.module.css";
import Head from "next/head";
import Image from "next/image";
import Layout from "../components/layout";
import movie_room from "../public/images/movie_room.jpg";

const Home = () => {
  return (
    <Layout home>
      <Head>
        <title>Maisons Cinema</title>
      </Head>
      <div className={styles.pageContent}>
        <Image src={movie_room} alt="movie room" layout="responsive" priority />
      </div>
    </Layout>
  );
};

export default Home;
