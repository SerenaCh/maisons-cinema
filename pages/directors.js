import Head from "next/head";
import Link from "next/link";
import Error from "next/error";
import { useRouter } from "next/router";
import styles from "../styles/Rubrique.module.css";
import Layout from "../components/layout";

const Directors = ({ directors, page }) => {
  const router = useRouter();

  const handlePreviousPage = (e) => {
    e.preventDefault();
    router.push(`directors?page=${Number(page) - 1}`);
  };

  const handleNextPage = (e) => {
    e.preventDefault();
    router.push(`directors?page=${Number(page) + 1}`);
  };

  return directors.length === 0 ? (
    <Error statusCode={503} />
  ) : (
    <Layout>
      <Head>
        <title>Liste de directeurs</title>
      </Head>
      <div className={styles.pageContent}>
        {directors !== undefined &&
          directors.map((director) => (
            <div key={director.id} className={styles.linkBloc}>
              <Link href={`directors/${director.id}`} passHref>
                <a className={styles.single}>
                  <div>{director.fullname}</div>
                </a>
              </Link>
            </div>
          ))}
        <div className={styles.buttons}>
          {page >= 2 ? (
            <a
              href={`directors?page=${Number(page) + -1}`}
              onClick={handlePreviousPage}
            >
              <div>Page précédente</div>
            </a>
          ) : (
            <a
              className={styles.hide}
              href={`directors?page=${Number(page) + -1}`}
              onClick={handlePreviousPage}
            >
              <div>Page précédente</div>
            </a>
          )}
          <div className={styles.buttons}>
            <a
              href={`directors?page=${Number(page) + 1}`}
              onClick={handleNextPage}
            >
              <div>Page suivante</div>
            </a>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query }) {
  let data;
  try {
    const response = await fetch(
      `${
        process.env.NEXT_PUBLIC_CINE_API
      }/rubrique/directeurs/limit/10/offset/${((query.page ?? 1) - 1) * 10}`
    );
    data = await response.json();
  } catch (error) {
    data = [];
  }

  return {
    props: { directors: data.persons, page: query.page ?? 1 },
  };
}

export default Directors;
