import Layout from "../components/layout";
import Head from "next/head";
import Link from "next/link";
import Error from "next/error";
import { useRouter } from "next/router";
import styles from "../styles/Rubrique.module.css";

const Actors = ({ actors, page }) => {
  const router = useRouter();

  const handlePreviousPage = (e) => {
    e.preventDefault();
    router.push(`actors?page=${Number(page) - 1}`);
  };

  const handleNextPage = (e) => {
    e.preventDefault();
    router.push(`actors?page=${Number(page) + 1}`);
  };

  return actors.length === 0 ? (
    <Error statusCode={503} />
  ) : (
    <Layout>
      <Head>
        <title>Liste d&apos;acteurs</title>
      </Head>
      <div className={styles.pageContent}>
        {actors !== undefined &&
          actors.map((actor) => (
            <div className={styles.linkBloc} key={actor.id}>
              <Link href={`actors/${actor.id}`} passHref>
                <a className={styles.single}>
                  <div>{actor.fullname}</div>
                </a>
              </Link>
            </div>
          ))}
        <div className={styles.buttons}>
          {page >= 2 ? (
            <a
              href={`actors?page=${Number(page) + -1}`}
              onClick={handlePreviousPage}
            >
              <div>Page précédente</div>
            </a>
          ) : (
            <a
              className={styles.hide}
              href={`actors?page=${Number(page) + -1}`}
              onClick={handlePreviousPage}
            >
              <div>Page précédente</div>
            </a>
          )}
          <a href={`actors?page=${Number(page) + 1}`} onClick={handleNextPage}>
            <div>Page suivante</div>
          </a>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query }) {
  let data;
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_CINE_API}/rubrique/acteurs/limit/10/offset/${
        ((query.page ?? 1) - 1) * 10
      }`
    );
    data = await response.json();
  } catch (error) {
    data = [];
  }

  return {
    props: { actors: data.persons, page: query.page ?? 1 },
  };
}

export default Actors;
