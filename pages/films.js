import Layout from "../components/layout";
import Head from "next/head";
import Link from "next/link";
import Error from "next/error";
import { getFilms } from "../api/films";
import styles from "../styles/Rubrique.module.css";

const Films = ({ films }) => {
  return Object.keys(films).length === 0 ? (
    <Error statusCode={503} />
  ) : (
    <Layout>
      <Head>
        <title>Liste de films</title>
      </Head>
      <div className={styles.pageContent}>
        {Object.keys(films) !== undefined &&
          Object.keys(films).map((filmId, index) => (
            <div className={styles.linkBloc} key={index}>
              <Link href={`films/${filmId}`} passHref>
                <a className={styles.single}>
                  <div>{films[filmId].title}</div>
                </a>
              </Link>
            </div>
          ))}
      </div>
    </Layout>
  );
};

export async function getStaticProps() {
  return { props: { films: await getFilms() } };
}

export default Films;
