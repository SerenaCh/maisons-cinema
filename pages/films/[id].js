import styles from "../../styles/Film.module.css";
import { getFilms } from "../../api/films";
import Layout from "../../components/layout";
import Head from "next/head";
import Image from "next/image";
import Error from "next/error";

const allFilms = getFilms();

const basePath = `${process.env.NEXT_PUBLIC_CINE_API}/images/film`;

const Film = ({ film }) => {
  return film.isObjEmpty || !film.content ? (
    <Error statusCode={503} />
  ) : (
    <Layout>
      <Head>
        <title>{film.title}</title>
      </Head>
      <div className={styles.pageContent}>
        <section className={styles.filmContainer}>
          <aside className={styles.aside1}>
            <Image
              src={`${basePath}/${
                film.content.pictures[0].content.name
                  ? film.content.pictures[0].content.name
                  : ""
              }`}
              alt={film.title}
              width="386"
              height="513"
              style={{ borderRadius: "1%" }}
            />
          </aside>
          <aside className={styles.aside2}>
            <div className={styles.titles}>
              <div>{film.title}</div>
            </div>
            <div>
              <p className={styles.description}>{film.content.description}</p>
            </div>
            <div className={styles.bottomAside}>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>DUREE</span>
                  <br />
                  <span>{film.content.movie_duration}&apos;</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>WEB</span>
                  <br />
                  <span>
                    <a href={film.content.official_website}>site officiel</a>
                  </span>
                </div>
              </div>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>TITRE ORIGINAL</span>
                  <br />
                  <span>{film.content.original_title}</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>ANNEE DE PRODUCTION</span>
                  <br />
                  <span>{film.content.production_year}</span>
                </div>
              </div>
            </div>
          </aside>
        </section>
      </div>
    </Layout>
  );
};

export async function getStaticPaths() {
  const films = await allFilms;
  const paths = Object.keys(films).map((filmId) => {
    return { params: { id: filmId.toString() } };
  });
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  const films = await allFilms;
  return {
    props: { film: films[params.id] },
  };
}

export default Film;
