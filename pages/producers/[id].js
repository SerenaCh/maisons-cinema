import styles from "../../styles/Person.module.css";
import Head from "next/head";
import Error from "next/error";
import Layout from "../../components/layout";
import Image from "next/image";
import Film from "../../components/film";
import {getPerson} from "../../api/person";

const basePath = `${process.env.NEXT_PUBLIC_CINE_API}/images/personne`;

const Producer = ({ producer }) => {

  return producer.isObjEmpty || !producer.content ? (
    <Error statusCode={503} />
  ) : (
    <Layout>
      <Head>
        <title>{producer.title}</title>
      </Head>
      <div className={styles.pageContent}>
        <section className={styles.personContainer}>
          <aside className={styles.aside1}>
            <Image
              src={`${basePath}/${
                producer.content.photo ? producer.content.photo : ""
              }`}
              alt={producer.title}
              width="386"
              height="513"
              style={{ borderRadius: "1%" }}
            />
          </aside>
          <aside className={styles.aside2}>
            <div className={styles.titles}>
              <div>{producer.content.profession}</div>
              <div>{producer.title}</div>
            </div>
            <div>
              <p className={styles.description}>
                {producer.content.commentaire}
              </p>
            </div>
            <div className={styles.bottomAside}>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>NATIONALITE</span>
                  <br />
                  <span>{producer.content.nationalite}</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>WEB</span>
                  <br />
                  <span>
                    <a href={producer.content.url_dbpedia}>dbpedia</a>
                  </span>
                </div>
              </div>
              <div>
                <div>
                  <div className={styles.line}></div>
                  <span>PAYS DE NAISSANCE</span>
                  <br />
                  <span>{producer.content.lieu_naissance}</span>
                </div>
                <div>
                  <div className={styles.line}></div>
                  <span>DATE DE NAISSANCE</span>
                  <br />
                  <span>{producer.content.date_naissance}</span>
                </div>
              </div>
            </div>
          </aside>
        </section>
        <section className={styles.filmsSection}>
          <div>Filmographie</div>
          <div className={styles.filmsCollection}>
            {Object.keys(producer.content.movies).map((movieId, index) => (
              <Film key={index} movieId={movieId} movie={producer.content.movies[movieId]}/>
            ))}
          </div>
        </section>
      </div>
    </Layout>
  );
};

export async function getStaticPaths() {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_CINE_API}/rubrique/producteurs`
  );
  const producers = await res.json();

  const paths = producers.persons.map((person) => ({
    params: { id: person.id.toString() },
  }));
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  return {
    props: { producer: await getPerson(params.id) },
  };
}

export default Producer;
