import Link from "next/link";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";

const basePath = `${process.env.NEXT_PUBLIC_CINE_API}/images/film`;

export default function ActionAreaCard({ movie, movieId }) {
  return (
    <Link href={`/films/${movieId}`} passHref>
      <Card sx={{ width: 146, marginBottom: "24px" }}>
        <CardActionArea>
          <CardMedia
            component="img"
            width="146"
            height="197"
            src={`${basePath}/${movie.content.pictures[0].content.name}`}
            alt={`${movie.title} poster`}
          />
        </CardActionArea>
      </Card>
    </Link>
  );
}
