import styles from "./layout.module.css";
import Header from "./header";
import Navbar from "./navbar";
import Footer from "./footer";
import Link from "next/link";

const Layout = ({ children, home }) => {
  return (
    <div>
      <Header />
      <Navbar />
      {!home && (
        <div className={styles.container}>
          <div className={styles.backToHome}>
            <Link href="/">
              <a>← Back to home</a>
            </Link>
          </div>
        </div>
      )}
      <main>{children}</main>
      <Footer />
    </div>
  );
};

export default Layout;
