import styles from "./navbar.module.css";
import Link from "next/link";

const NavBar = () => {
  return (
    <div className={styles.navBar}>
      <div className={styles.navLinks}>
        <Link href="/actors">
          <a>
            <div>Acteurs</div>
          </a>
        </Link>
        <Link href="/directors">
          <a>
            <div>Directeurs</div>
          </a>
        </Link>
        <Link href="/producers">
          <a>
            <div>Producteurs</div>
          </a>
        </Link>
        <Link href="/films">
          <a>
            <div>Films</div>
          </a>
        </Link>
      </div>
      <div className={styles.line}></div>
    </div>
  );
};

export default NavBar;
