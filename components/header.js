import styles from "./header.module.css";
import Link from "next/link";
import Image from "next/image";
import logo from "../public/images/logo.png";

const Header = () => {
  return (
    <div className={styles.header}>
      <Link href="/">
        <a>
          <Image
            src={logo}
            alt="logo"
            width="180"
            height="100"
            layout="fixed"
          />
        </a>
      </Link>
      <div className={styles.line}></div>
    </div>
  );
};

export default Header;
