export const getPerson = async (id) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_CINE_API}/personne/${id}`
    );
    return await response.json();
  } catch (error) {
    return {};
  }
};
