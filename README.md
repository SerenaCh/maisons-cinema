# Maisons Cinema

Maisons Cinema is a cinema network website developed with next.js (SSR)

## How to run

- Git clone the project : `$ git clone git@gitlab.com:SerenaCh/maisons-cinema.git`
- You will need to create .env file. A template exist under template.env
- Install node-js: `https://nodejs.org/en/download/`
- Install dependencies: `$ yarn` or `$ npm i`
- Build with : `$ npm run build`
- Run with :  `$ npm run start`